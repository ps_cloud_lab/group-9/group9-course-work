##   Cloud adoption framework (CAF)
Best practices to help organizations build good approach to accelerate successful cloud adoption
### Organized into 6 perspectives
* Business : Stakeholders(business managers, finance managers etc) use CAF to create strong business case for cloud adoption that aligns with IT strategies and goals.  

* People : Stakeholders from people resources( human resources, staffing) can use CAF to evaluate organizational structures and roles and new skill requirements.
* Governance : Stakeholders( Chief information office, program managers, enterprise architects, BA) can use CAF to focus on the skills and processes that are needed to align IT strategy and goals with business strategy and goals.
* Platform : Stakeholders (Chief tech officer, IT managers, solution architects) use a variety of models to understand and communicate the nature of IT systems and their relationships.CAF includes patterns for migrating on-premises workloads to the cloud.
* Security : Stakeholders (Chief Information Security Officer, IT security managers) can use CAF to structure the implementation of security controls that meet the organizations needs.
* Operations : IT operations mangers, It support managers can use CAF to identify the process changes and training that are needed to implement successful cloud adoption.
  
## AWS Well_architected Framework 6 Pillars
Helps you design using 5 pillars.
Operational Excellence, security, reliability, performance efficiency and cost optimization.
### 1. Operational Excellence:  
 Monitoring system to deliver business value. 
* Perform operations as code
* Annotate documentation
* Make frequent,small, reversible changes
* Refine operations procedures frequently
* Anticipate failure
* Learn from all operational failures
### 2. Security:  
 Protecting confidentiality and integrity of data
 * Apply security at all layers, at the perimeter and within and between resources
 * Enable traceability through logging and auditing
 * Implement principle of least privilege
 * Secure system with shared responsibility model. 
 * Automate security best practices, save a patched, hardened image of a virtual server so that when you need an image you can use it to automatically create a new instance
 ### 3.Reliability:  
  Ability of the system to recover from infrastructure failure and meet the demand. Ability of a work load to perform its intended function correctly when it's expected to.
  * Automatically recover from failure, by monitoring the performance of services 
  * Test recovery procedures by simulating different failure's or recreating failure scenarios
  * Scale horizontally to increase aggregate workload availability
  * stop guessing capacity
  * Manage change in automation, all changes can be tracked and automated
### 4.Performance Efficiency  
Ability to use computing resources efficiently to meet system requirements and to maintain that efficiency as demand changes and technologies evolve.
### 5.Cost Optimization
Ability to run systems to deliver business value at the lowest value point.
### 6. Sustainability
 ![](AwsDiagrams.png)
 
 High availability 
is achieved by using multiple servers, isolated redundant data centers within each Availability Zone,
multiple regions around the world,fault tolerant services, multiple AZ in a region.
 System operations  
 includes the development of reusable infrastructure templates, which are then tested, deployed, monitored and safeguarded.
 Its is a good practice to use a file or application to package the description of the network and the components that will run in the network

### IAM
4 policy types  
* Identity- based policies : allows user to attach managed & inline policies to users, groups, roles 
* Resource based : They are JSON documents ie S3 bucket policies  
                 IAM role trust policies eg list specific instances and specific EBS volumes
                 Do not always allow all actions   
                 Actions such as run instances cannot be specified as it is nor possible to know the InstanceID before the run instances call is complete
                   
* AWS organizations service control policies(SCPs) : Apply permission boundaries to AWS organizations or organizational units(OUs)                   
* Access control lists(ACLS) : Only policy type that do not use JSON policy document structure. similar to resource  
 based policies. can be used to control which users or resources can access a resource.
  
  Use roles to provide cross-account access  
A *cross- account access* :grants access from another AWS account
  
  *external ID* : use is a best practice as it stops one AWS account from guessing the role ARN(Amazon resource name) of another account and access can be granted without sharing permanent security credentials.
 ### CLI format
 $ pip3 install aws cli --upgrade -user
  -user switch is used to place the AWS cli install in ~/.local/bin
  $ aws --version
  $ aws help
  $ aws ec2 help
  $ aws ec2 describe-instance help
The *$ run-instance* subcommand is used to request the creation of a new EC2 instance.
Limiting results: --query option
aws ec2 describe-instances --query 'Reservations[0].Instances[0]
-- filter option
aws ec2 describe-instances --filter "Name=platform, Values=windows"
Return results are filtered so that only instances that run Windows Server are displayed
### AWS Instances
An instance can be rebooted without restarting the billing cycle. However if an instance is stopped and then restarted a new billing hour begins immediately on restart.
On restarting an instance a new IP address is received while if you only reboot an instance the public address is retained.  

Elastic IP address's are *associated* with an instance even if it is stopped but if an instance is terminated the elastic address becomes disassociated from an instance.
### Hibernation:   
Certain instance types support hibernation. Specific M,R and C series instances that run AMazon Linux 1.  
To enable it must be configured at launch by using AWS Management Console or CLI
### Update instances
* MicrosfoWindows automatically updates instances through Windows update.
* YUM Yellowdog updater, Modified is an open source, Redhat Package manager(RPM)-based package installer for Linux.
* AWS Systems Manager and AWS OpsWorks services offer features that makes EC2 instance updates easy.
### Lambda
* fully managed
* event driven
* running time of a function is limited to maximum 15 minutes
* multiple languages supported.
* pay only for the compute time consumed
any code can be run for virtually any type of application or service and Lambda handles it all.

### Internet gateway:  
 An internet gateway is horizontally scaled and highly available VPC component that makes communication between instances in the VPC and the internet possible.
 ### VPN(Virtual private gateway)  
 A VPN is optional and is attached to a VPC where you want to create a VPN connection. 
 The  * Customer side of the VPN connection has a customer gateway, which is a physical device or a software application.
 
 ### Security Groups: 
 They are stateful firewalls, a firewall that monitors the full state of active network connections. Multiple security groups can be assigned to a single instance. ie administrative security group which will allow traffic on TCP port 22. Database server security group that will allow traffic on port 3306
 A *bastian host* is an instance that can connect to other instances that are otherwise unreachable from outside the VPC
 A Security Group acts as a virtual firewall for the EC2 instance to control incoming and outgoing traffic. Inbound rules control the incoming traffic to your instance, and outbound rules control the outgoing traffic from your instance. Security groups are the responsibility of the customer
 ### AWS Shield  
  is a managed service that protects against *Distributed Denial of Service* (DDoS) attacks for applications running on AWS. AWS Shield Standard is enabled for all AWS customers at no additional cost. AWS Shield Standard automatically protects your web applications running on AWS against the most common, frequently occurring DDoS attacks. You can get the full benefits of AWS Shield Standard by following the best practices of DDoS resiliency on AWS. As Shield Standard is automatically activated for all AWS customers with no options for any customizations, therefore AWS needs to manage the maintenance and configurations for this service. 
### AWS Web Application Firewall (WAF)  
 *Fully managed* service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale. APIs act as the "front door" for applications to access data, business logic, or functionality from your backend services. 
  AWS WAF is a web application firewall that lets you monitor the HTTP and HTTPS requests that are forwarded to an API Amazon Gateway, Amazon CloudFront or an Application Load Balancer. AWS WAF also lets you control access to your content. AWS WAF has to be *enabled by the customer* and comes under the customer’s responsibility.
### Amazon Macie  
 Amazon Macie is a *fully managed data security and data privacy service* that uses machine learning and pattern matching to discover and protect your sensitive data in AWS. Macie helps identify and alert you to sensitive data, such as personally identifiable information (PII). 

=======


 ### Cloud Storage
 
Cloud storage is a service model that allows data to be stored remotely through a cloud computing provider, where data is maintained, managed, backed up, and made available to users over a network

### Benefits of Cloud Storage
 - Secure Backup Storage
 - Remove the need for costly infrastructure
 - Enhanced Security
 - Scalable Service
 - Increased Productivity
 - Greater Accessibility

### Cloud Storage Categories

- Block storage   
   - Amazon Elastic Block Store (Amazon EBS)     
- Object storage   
  -  Amazon Simple Storage Service (Amazon S3 
  - Amazon Simple Storage Service Glacier   
- File storage   
  - Amazon Elastic File System (Amazon EFS)   
  - Amazon FSx   
- Hybrid cloud storage   

## Amazon Elastic Block Store (Amazon EBS)
- EBS is a high performance, non-volatile block storage for computer instances in AWS
- EBS provides persistent block-level storage volumes for use with Amazon EC2 instances 
- Block storage is the type of storage that places data in a block, each block has a unique identifier.
- EBS volumes are mounted, and they’re used directly by the host, just like a virtual hard drive
- EBS volume is automatically replicated within its availability zones to protect from component failure to offer high availability and durability.
- EBS is flexible storage, it can be used with multiple types of operating systems such as Windows and Linux.


### Amazon EBS volume type
- Provisioned IOPS SSD (io1)
- General Purpose SSD (gp2)
- Throughput Optimized HDD (st1)
- Cold HDD (sc1)


## Amazon Elastic File Storage (EFS)

Amazon Elastic File Storage  is a fully managed cloud-native file system.    

EFS file systems support thousands of concurrent clients. it is designed to be used by many computing instances at the same time.    

It is elastic and can grow on demand to petabytes in size and it supports NFS.   

EFS is file system storage for Linux-based workloads that can be used with AWS cloud services and on-premises resources.  

EFS offers multiple storage classes as well as life cycle policies for cost optimization, these Storage classes include:
- 	Standard 
- 	Infrequent Access  

EFS has two performance modes which are:
- 	General Purpose 
- 	Max I/O  

Two options for throughput mode exist:
- Bursting Throughput
- Provisioned Throughput  

EFS use cases include:
- Home directories
-	File system for enterprise applications
- Application testing and development
- Database backups
- Web serving and content management
- Big Data analytics
-  Media workflows 



### Snapshot  
Snapshot is a duplicate or replica of an EBS volume that can be stored as a backup of the volume. 
### Systems Manager

AWS Systems manager which helps with the maintenance of resources and also helps to identify issues that may effect the applications.The users can identify the resources they want to manage and set them accordingly to run tasks across all the instances.It can be used to apply operating system patches and create system images. Its highly automation focussed that helps with the management of systems that run on premises or on AWS.