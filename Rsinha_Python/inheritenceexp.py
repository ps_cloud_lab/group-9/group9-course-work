class Movies:
    def __init__(self, movie_name, actor_name):
        self.movie_name = movie_name
        self.actor_name = actor_name
    
    def show(self):
        print(self.movie_name, self.actor_name)

obj = Movies("Lawrence of Arabia","Alec Guinness")
obj.show()


